import React, { useState } from "react";
import { Alert, Button } from "react-bootstrap";

const Demo = () => {


    const [json, setJson] = useState<{ text: string, error: boolean }>({ text: '', error: false });
    const get = () => {
        try {
            fetch(`${process.env.REACT_APP_BACKEND_URL || ''}/api/react`).then(async response => {
                const j = await response.json();
                console.log(j);
                setJson({ text: JSON.stringify(j), error: false });
            }).catch(e=>{
                console.log(JSON.stringify(e));
                setJson({ text: e.toString(), error: true });    
            });
        } catch (e) {
            setJson({ text: JSON.stringify(e), error: true });
        }
    }

    const put = () => {

        fetch(`${process.env.REACT_APP_BACKEND_URL || ''}/api/react`, { method: 'put' }).then(async response => {
            alert('success');
        });
    }

    return <>
        <Button variant="primary"
            onClick={get}>
            Я GET </Button>

        <Button variant="primary"
            onClick={put}>
            put </Button>
        {!json.error && json.text && <Alert variant={'primary'}>
            {json.text}
        </Alert>}

        {json.error && json.text && <Alert variant={'error'}>
            {json.text}
        </Alert>}
    </>;
}

export default Demo;