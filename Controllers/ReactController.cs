﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.ReactWithAsp.Controllers
{
	[Route("/api/react")]
	public class ReactController : Controller
	{
		[HttpGet]
		public object Index()
		{
			return new { Ok = true, Date = DateTime.Now };
		}

		[HttpPut]
		public void Post()
		{

		}
	}
}
