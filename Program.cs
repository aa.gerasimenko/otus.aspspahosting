using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Otus.ReactWithAsp
{
	public class Program
	{
		public static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
			// �������, ��� ���������� ����� ������� ��� ���-������
			.UseWindowsService()
			.ConfigureAppConfiguration((hostingContext, config) =>
			{
				var isService = !(Debugger.IsAttached || args.Contains("--console"));
				if (isService)
				{

					var processModule = Process.GetCurrentProcess().MainModule;
					if (processModule != null)
					{

						// �������������, ��� ������� ����� ���������� 
						// ������� �����, � �� C:\windows\system32
						config.SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
						// ��������� ���� ��� ����� ��������� ��������� ����������
						// �������� ����� ��������
						config.AddJsonFile("hostsettings.json", optional: true);
					}
				}
			})
				.ConfigureWebHostDefaults(webBuilder =>
					webBuilder.UseStartup<Startup>());
	}
}
